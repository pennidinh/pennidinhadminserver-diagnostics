const safeJsonStringify = require('safe-json-stringify');
const AWS = require('aws-sdk');
// Set the Region
AWS.config.update({region: 'us-west-2'});
const s3 = new AWS.S3({apiVersion: '2006-03-01'});
const { v4: uuidv4 } = require('uuid');

const succeed = function(callback, body) {
    callback(null, {'headers': {'access-control-allow-origin': '*'}, 'statusCode': 200, 'body': (body instanceof String ? body : safeJsonStringify(body))});
}

const fail = function(callback, body, prefix) {
    callback(null, {'headers': {'access-control-allow-origin': '*'}, 'statusCode': 400, 'body': (prefix ? prefix : '') + (body instanceof String ? body : safeJsonStringify(body))});
}

const fault = function(callback, body, prefix) {
    callback(null, {'headers': {'access-control-allow-origin': '*'}, 'statusCode': 500, 'body': (prefix ? prefix : '') + (body instanceof String ? body : safeJsonStringify(body))});
}

exports.handler = function(event, context, callback) {
    console.log('Handling lambda invocation...');

/*
* sample event object:
* 
* 2020-08-15T17:22:39.934Z	3fa6a4af-18bb-497e-8f88-eaa6a18a062b	INFO	Event: 
* {
*     "version": "2.0",
*     "routeKey": "$default",
*     "rawPath": "/Prod/isAlive",
*     "rawQueryString": "clientId=12321",
*     "headers": {
*         "content-length": "0",
*         "host": "diagnostics.pennidinh.com",
*         "user-agent": "curl/7.54.0",
*         "x-amzn-trace-id": "Root=1-5f3819df-c82134e3f8a2f1bb73b78fc7",
*         "x-forwarded-for": "73.167.111.89",
*         "x-forwarded-port": "443",
*         "x-forwarded-proto": "https"
*     },
*     "queryStringParameters": {
*         "clientId": "12321"
*     },
*     "requestContext": {
*         "accountId": "334099949173",
*         "apiId": "cnlkwt2wn3",
*         "domainName": "diagnostics.pennidinh.com",
*         "domainPrefix": "diagnostics",
*         "http": {
*             "method": "GET",
*             "path": "/Prod/isAlive",
*             "protocol": "HTTP/1.1",
*             "sourceIp": "73.167.111.89",
*             "userAgent": "curl/7.54.0"
*         },
*         "requestId": "RUj6_hzmPHcEMpw=",
*         "routeKey": "$default",
*         "stage": "Prod",
*         "time": "15/Aug/2020:17:22:39 +0000",
*         "timeEpoch": 1597512159890
*     },
*     "isBase64Encoded": false
* }
*
* Sample context object:
*
* {
*     "callbackWaitsForEmptyEventLoop": true,
*     "functionVersion": "$LATEST",
*     "functionName": "PenniDinhCentral-Diagnostics",
*     "memoryLimitInMB": "128",
*     "logGroupName": "/aws/lambda/PenniDinhCentral-Diagnostics",
*     "logStreamName": "2020/08/15/[$LATEST]6649d54877dd4eac8fafcd275977bf02",
*     "invokedFunctionArn": "arn:aws:lambda:us-west-2:334099949173:function:PenniDinhCentral-Diagnostics",
*     "awsRequestId": "3fa6a4af-18bb-497e-8f88-eaa6a18a062b"
* }
*/

    const path = event.rawPath;
    console.log('path: ' + path);
    const sourceIp = event.requestContext.http.sourceIp;

    if (path === '/Prod/envFile') {
       console.log(sourceIp + ' .env file: ' + Buffer.from(event.body, 'base64'));
       succeed(callback, 'ok');
    } else if (path === '/Prod/pubKey') {
       console.log(sourceIp + ' pub key file: ' + Buffer.from(event.body, 'base64'));
       succeed(callback, 'ok');
    } else if (path === '/Prod/dockerComposeTop') {
       const body = event.body;
       s3.putObject({
        Bucket: 'pennidinh-diagnostics-reports',
        Key: 'top-' + new Date().toISOString() + '-' + sourceIp + '-' +  uuidv4() + '.txt',
        ContentType:'text/plain',
        Body: Buffer.from(body, 'base64')
      }, function (err, data) {
        if (err) {
          console.log(err);
          fault(callback, 'Error writing file to S3: ' + err);
          return;
        }

        succeed(callback, 'ok');
      });
    } else if (path === '/Prod/dockerComposeLogs') {
       const body = event.body;
       s3.putObject({
        Bucket: 'pennidinh-diagnostics-reports',
        Key: 'logs-' + new Date().toISOString() + '-' + sourceIp + '-' +  uuidv4() + '.txt',
        ContentType:'text/plain',
        Body: Buffer.from(body, 'base64')
      }, function (err, data) {
        if (err) {
          console.log(err);
          fault(callback, 'Error writing file to S3: ' + err);
          return;
        }

        succeed(callback, 'ok');
      });
    } else {
       fail(callback, 'Unknown API: ' + path);
    }
}
